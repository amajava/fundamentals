package com.aep.training;

public class Main {

    public static void main(String[] args) throws Exception{
        /**
         *    WithoutObjects test1= new WithoutObjects();
         test1.Add("Emrah","PEKESEN");
         test1.Add("Fatih","ÖZGEN");
         test1.Add("Zeynep","CEBECİ");

         System.out.println(test1.Search("ra"));
         */


        StudentService service = new StudentService();
        service.Add(new Student("Emrah","PEKESEN","a@b.com"));
        service.Add(new Student("Fatih","ÖZGEN","a@d.com"));
        service.Add(new Student("Zeyno","CEBECİ","a@c.com"));

        System.out.println(service.SearchByEmail("Emrah"));
    }
}
