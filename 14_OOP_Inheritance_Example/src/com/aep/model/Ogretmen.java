package com.aep.model;

public class Ogretmen extends Insan {

    String school;
    String courseName;

    public Ogretmen(String _name,String _surname,String _school,String _courseName){
        this.name=_name;
        this.surname=_surname;
        this.school=_school;
        this.courseName=_courseName;
    }

    @Override
    public void introduceMe() {
        String message = "Merhaba ben "+this.name+" "+this.surname+"."+this.school+" Okulunda "+this.courseName+" öğretmeniyim";
        System.out.println(message);
        super.giveLocation();
    }
}
