package com.aep.training;

import java.util.Scanner;

public class Emrah {

    public static void main(String[] args) {
        readDataFromUser();

        /**
         *  System.out.println("Program Başladı..");
         System.out.println("---------------------------------");

         String mesaj1 = selamla("Emrah","PEKESEN");
         String mesaj2 = selamla("Sibel","CANIAZ");

         System.out.println(mesaj1);
         System.out.println(mesaj2);
         */
    }

    public static void readDataFromUser(){
        Scanner reader = new Scanner(System.in);
        String isim,soyisim,mesaj;

        System.out.print("Lütfen Adınızı Giriniz    : ");
        isim = reader.nextLine();
        System.out.print("Lütfen Soyadınızı Giriniz : ");
        soyisim = reader.nextLine();

        mesaj = selamla(isim,soyisim);

        System.out.println(mesaj);
    }

    public static String selamla(String isim,String soyisim){
        return "Hoşgeldiniz " + isim + " " + soyisim +".";
    }



    public static void moveRight(){
        write("->");
    }

    public static void moveLeft(){
        write("<-");
    }

    public static void moveUp(){
        write("^\n");
        write("|");
    }

    public static void moveDown(){
        write("|\n");
        write("v");
    }

    public static void write(String message){
        System.out.print(message);
    }

    public static void havaDurumuVer(){
        System.out.println("Bugün Hava Bulutlu..");
    }

    public static void SayHello(){
        System.out.println("Hello Canım..");
    }

    public static String SayAleykumSelam(){
        return "Aleykum Selam";
    }

}
