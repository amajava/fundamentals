package com.aep.training;

import java.util.ArrayList;
import java.util.List;

public class StudentService {

    private List<Student> ogrenciler = new ArrayList<>();

    public void Add(Student ogrenci){
        this.ogrenciler.add(ogrenci);
    }

    public String Search(String input){
        String message="Aradığınız kayıt bulunamadı!";
        for (Student ogrenci:this.ogrenciler ) {
            if(ogrenci.getName().contains(input) || ogrenci.getSurname().contains(input) || ogrenci.getEmail().contains(input)){
                message = ogrenci.introduceYourSelf();
                break;
            }
        }
        return message;
    }

    public String SearchByEmail(String input){
        String message="Aradığınız kayıt bulunamadı!";
        for (Student ogrenci:this.ogrenciler ) {
            if(ogrenci.getEmail().contains(input)){
                message = ogrenci.introduceYourSelf();
                break;
            }
        }
        return message;
    }
}
