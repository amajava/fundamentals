package com.aep.training;

import java.util.Scanner;

public class Main {

    public static  Scanner okuyucu = new Scanner(System.in);
    public static  int s1,s2,sonuc;

    public static void main(String[] args) {
       byte secim;
       boolean devamEdeyimmi = true;


       while(devamEdeyimmi) {
           secim =  MenuOlustur();
           if (secim >= 1 && secim <= 5) {
               if(secim==5){
                   devamEdeyimmi = false;
                   continue;
               }
               SayilariIste();
               if (secim == 1) {
                   Topla();
               } else if (secim == 2) {
                   Cikar();
               } else if (secim == 3) {
                   Carp();
               } else {
                   Bol();
               }
               SatırYaz("İşleminizin Sonucu : " + sonuc);
           } else {
               SatırYaz("Lütfen 1-5 Arası Bir Seçim Yapınız!");
           }
       }
        SatırYaz("Yine Bekleriz..");
    }

    public static void SayilariIste(){
        System.out.print("Lütfen 1. Sayıyı Giriniz : ");
        s1 = okuyucu.nextInt();
        System.out.print("Lütfen 2. Sayıyı Giriniz : ");
        s2 = okuyucu.nextInt();
    }

    public static void Topla(){
        sonuc = s1+s2;
    }
    public static void Cikar(){
        sonuc = s1-s2;
    }
    public static void Carp(){
        sonuc = s1*s2;
    }
    public static void Bol(){
        sonuc = s1/s2;
    }

    public static byte MenuOlustur(){

        SatırYaz("******Menü******");
        SatırYaz("*  1- Topla    *");
        SatırYaz("*  2- Çıkar    *");
        SatırYaz("*  3- Çarp     *");
        SatırYaz("*  4- Böl      *");
        SatırYaz("*  5- Çıkış    *");
        SatırYaz("****************");
        Yaz("Seçiminiz : ");

        return okuyucu.nextByte();
    }

    public static void Yaz(String mesaj){
        System.out.print(mesaj);
    }

    public static  void SatırYaz(String mesaj){
        Yaz(mesaj+"\n");
    }
}
