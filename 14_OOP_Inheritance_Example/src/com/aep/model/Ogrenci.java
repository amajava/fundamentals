package com.aep.model;

import java.util.ArrayList;
import java.util.List;

public class Ogrenci extends Insan {

    private String school;
    private List<String> listOfCourses;

    public Ogrenci(String _name,String _surname,String _school){
        this.name=_name;
        this.surname=_surname;
        this.school=_school;
        this.listOfCourses = new ArrayList<String>();
    }

    public void addCourse(String courseName){
        this.listOfCourses.add(courseName);
    }

    @Override
    public void introduceMe() {
        String message = "Merhaba ben "+this.name+" "+this.surname+"."+this.school+" Okulunda öğrenciyim.";
        if(this.listOfCourses.size()>0) {
            message += "\nAldığım Dersler :";
            for (String s : this.listOfCourses) {
                message += "\n" + s;
            }
        }
        else {
            message+="\nHiç Bir Ders Almıyorum!";
        }
        System.out.println(message);
        super.giveLocation();
    }
}
