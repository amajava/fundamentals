package com.aep.training;

public class Main {

    public static void main(String[] args) {
	    for(int i = 1 ; i<100; i++){

            if(i%2==1){
                continue;
            }

            if(i==50){
                break;
            }

            if(i%44==0){
                return;
            }

            System.out.println(i);
        }

        System.out.println("Program Düzgünce Bitti. Byes...");
    }
}
