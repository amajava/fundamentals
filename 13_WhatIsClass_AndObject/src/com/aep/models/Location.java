package com.aep.models;

public class Location {
    int X;
    int Y;

    public Location(int _x,int _y)
    {
        this.X=_x;
        this.Y=_y;
    }

    public void tellMe(){
        System.out.println(this.X+","+this.Y);
    }
}
