package com.aep.training;

import java.util.Scanner;

public class Main {

    //public static String[] yemekTarifleri = new String[5];
    public static String[] yemekTarifleri = {"Menemen:domates,yumurta,biber"
                                            ,"Salata:kıvırcık,havuç,salatalık"
                                            ,"Cacık:yoğurt,salatalık,nane"};



    public static void main(String[] args)throws InterruptedException {
        /**
         * 1-> Kullanıcıdan Bir Malzeme İste.
         * 2-> Bu malzeme db de varmı bak.
         * 2-a-> Yoksa Mesaj Ver Programı Sonlandır.
         * 2-b-> Varsa, ilgili yemeğin adını bul ve bir değişkene ata.
         * 3-> Bulunan Yemeği kullanıcıya göster.
         * 4-> Programı sonlandır.
         */

        String kullanidanAlinanData = kullaniciyaMalzemeSor();
        String bulunanYemek = dbdeAra(kullanidanAlinanData);

        if(bulunanYemek!=null){
            System.out.println("Size En Uygun Yemek : "+bulunanYemek);
        }
    }

    public static String kullaniciyaMalzemeSor() throws InterruptedException{
        Scanner reader = new Scanner(System.in);
        String arananKriter="";
        System.out.print("Lütfen elinizdeki malzemeyi yazınız : ");
        arananKriter = reader.nextLine();
        System.out.println("------------------------------------");
        System.out.print("Yemek Aranıyor");
        for (int sayac = 0; sayac<10; sayac++) {
            Thread.sleep(100);
            System.out.print(".");
        }
        System.out.println("\n------------------------------------");
        return arananKriter;
    }

    public static String dbdeAra(String kriter){
        String bulunanYemekSatiri ="";
        String bulunanYemekAdi="";
        for (int i = 0; i < yemekTarifleri.length ; i++) {
            if(yemekTarifleri[i].contains(kriter)){
                bulunanYemekSatiri = yemekTarifleri[i];
                break;
            }
        }

        if(bulunanYemekSatiri==""){
            System.out.println("Bu malzeme ile bir yemek bulamadık!");
            return null;
        }else{
            bulunanYemekAdi = bulunanYemekSatiri.split(":")[0];
        }
        return bulunanYemekAdi;
    }
}
