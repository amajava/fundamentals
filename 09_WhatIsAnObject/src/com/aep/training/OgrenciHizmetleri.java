package com.aep.training;

public class OgrenciHizmetleri {

    private static Ogrenci[] ogrenciler = new Ogrenci[10];
    private static byte counter = 0;

    public void Ekle(Ogrenci ogr) {
        if (counter == 10) {
            System.out.println("Ogrenci Dizisi Dolu!");
            return;
        }

        ogrenciler[counter++] = ogr;
    }

    public void Listele() {
        System.out.println("Öğrenci Listesi ");
        System.out.println("-------------------------------------");
        if (counter != 0) {
            for (Ogrenci ogr : ogrenciler) {
                if(ogr!=null) {
                    System.out.println(ogr);
                }
            }
        }else{
            System.out.println("Liste Boş!");
        }
    }
}
