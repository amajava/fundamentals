package com.aep.training;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String isim;
        String soysim;
        byte yas;

        Scanner reader = new Scanner(System.in);

        /* *
         * System.out.print("Lütfen Adınızı,Soyadını,Yaşınızı Giriniz.. : ");
         isim = reader.nextLine();
         soysim = reader.nextLine();
         yas = reader.nextLine();
         */


        System.out.print("Lütfen Adınızı Giriniz.. : ");
        isim = reader.nextLine();
        System.out.print("Lütfen Soyadınız Giriniz.. : ");
        soysim = reader.nextLine();
        System.out.print("Lütfen Yaşınızı Giriniz.. : ");

        yas = reader.nextByte();
        if(yas>=18) {
            System.out.println("Merhaba " + isim + " " + soysim + ". Sisteme Hoşgeldin.");
        } else{
            System.out.println("Merhaba " + isim + " " + soysim + ". Sisteme Giremezsin Yaşın Küçük!");
        }

        /*
        try {
            yas = reader.nextByte();
            if(yas>=18) {
                System.out.println("Merhaba " + isim + " " + soysim + ". Sisteme Hoşgeldin.");
            } else{
                System.out.println("Merhaba " + isim + " " + soysim + ". Sisteme Giremezsin Yaşın Küçük!");
            }
        }catch(InputMismatchException ex){
            System.out.println("Lütfen 0-100 Arası Bir Yaş Giriniz!");
        }
        */



    }
}
