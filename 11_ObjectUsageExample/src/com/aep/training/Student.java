package com.aep.training;

public class Student {

    private String name;
    private String surname;
    private String email;

    public Student(String _name,String _surname,String _email){
        this.name=_name;
        this.surname=_surname;
        this.email=_email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String introduceYourSelf(){
        return "Hello, I'm "+this.name+" "+this.surname+".My email adress is "+this.email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
