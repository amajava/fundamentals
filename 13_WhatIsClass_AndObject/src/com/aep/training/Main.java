package com.aep.training;

import com.aep.models.*;

public class Main {

    public static void main(String[] args) {

        Car araba1 = new Car("00001");

        araba1.forward();
        araba1.forward();
        araba1.forward();
        araba1.back();
        araba1.stop();

        Car araba2 = new Car("00002");

        araba2.forward();
        araba2.forward();
        araba2.forward();
        araba2.forward();
        araba2.forward();
        araba2.stop();
        araba2.back();
        araba2.back();
        araba2.stop();

    }
}
