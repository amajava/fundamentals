package com.aep.training;

public class Ogrenci {

    private String isim,soyisim;
    private boolean cinsiyet;
    private byte yas;

    public Ogrenci(String _isim,String _soyisim,boolean _cinsiyet,byte _yas){
        this.isim = _isim;
        this.soyisim =_soyisim;
        this.cinsiyet=_cinsiyet;
        this.yas=_yas;
    }



    public String toString(){
        return "Merhaba, ben "+isim+" "+soyisim+". Yaşım "+yas+".";
    }

}
