package com.aep.training;

public class Bird extends Animal {

    @Override
    public void goForward() {
        System.out.println("Flying");
    }
}
