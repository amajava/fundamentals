package com.aep.model;

public abstract class Insan {

    String name;
    String surname;
    String age;
    String gender;

    Location location = new Location(0,0);

    public abstract void introduceMe();

    public void giveLocation(){
        System.out.println("Location : "+this.location.giveLocation());
    }
}
