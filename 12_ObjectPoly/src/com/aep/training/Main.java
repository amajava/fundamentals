package com.aep.training;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Animal> animals = new ArrayList<>();
        animals.add(new Bird());
        animals.add(new Dog());
        animals.add(new Bird());
        animals.add(new Fish());
        animals.add(new Bird());
        //animals.add(new Animal());

        for (Animal a : animals ) {
            a.goForward();
        }
    }
}
