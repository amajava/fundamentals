package com.aep.models;

public class Car {

    String make;
    String model;
    String cc;
    String hp;
    String chasisNo;

    Location location;

    public Car(String _chasisNo){
        this.location = new Location(0,0);
        this.chasisNo = _chasisNo;
    }

    public void forward(){
        System.out.print("Car "+this.chasisNo+" is going 1 step forward ! Current Location : ");
        this.location.X++;
        this.location.tellMe();
    }

    public void back(){
        System.out.print("Car "+this.chasisNo+" is going 1 step backward ! Current Location : ");
        this.location.X--;
        this.location.tellMe();
    }


    public void stop(){
        System.out.print("Car "+this.chasisNo+" is stopped!\n");
        System.out.print("Last Location Info : ");
        this.location.tellMe();
    }


}
