package com.aep.training;

import com.aep.model.Insan;
import com.aep.model.Ogrenci;
import com.aep.model.Ogretmen;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Ogrenci o1 = new Ogrenci("Ali Emrah","PEKESEN","Okyanus Koleji");
        Ogrenci o2 = new Ogrenci("Kıvanç","PEKESEN","Okyanus Koleji");
        Ogrenci o3 = new Ogrenci("Aslı","PEKESEN","Okyanus Koleji");

        Ogretmen o4 = new Ogretmen("Haydar","PEKESEN","Okyanus Koleji","Matematik");
        Ogretmen o5 = new Ogretmen("Asiye","PEKESEN","Okyanus Koleji","İngilizce");

        o1.addCourse("Matematik");
        o1.addCourse("Türkçe");
        o1.addCourse("Fen Bilgisi");

        o2.addCourse("Matematik");
        o2.addCourse("Beden Eğitimi");

        List<Insan> insanlar = new ArrayList<>();
        insanlar.add(o1);
        insanlar.add(o2);
        insanlar.add(o3);
        insanlar.add(o4);
        insanlar.add(o5);

        for (Insan insan:
             insanlar) {
            insan.introduceMe();

        }
    }
}
