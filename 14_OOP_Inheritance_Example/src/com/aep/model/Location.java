package com.aep.model;

public class Location {

    private int X;
    private int Y;

    public Location(int _x,int _y){
        this.X=_x;
        this.Y=_y;
    }

    protected String giveLocation(){
        return this.X+","+this.Y;
    }
}
